(set-env!
 :source-paths #{"src"}
 :resource-paths #{"resources"}
 :dependencies '[[perun "0.4.3-SNAPSHOT" :scope "test"]
                 [hiccup "1.0.5" :exclusions [org.clojure/clojure]]])

(require '[io.perun :as p])

(task-options!
  pom {:project 'com.trevortoday
       :version "0.0.1"}
  p/render {:renderer 'com.trevortoday.core/page})

(deftask build []
  (comp (p/markdown)
        (p/render)
        (sift :include #{#"^public/"})
        (sift :move {#"^public/" ""})
        (target)))
