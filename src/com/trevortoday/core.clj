(ns com.trevortoday.core
  (:require [hiccup.page :as hp]))

(defn page [data]
  (hp/html5
   [:head 
    [:title "Trevor Today"]
    [:meta {:charset "utf-8"}]
    [:meta {:name "description" :content "This is Trevor Today"}]
    [:meta {:property "og:title" :content "Trevor Today"}]
    [:meta {:property "og:description" :content "This is Trevor Today"}]
    [:meta {:property "og:url" :content "https://trevortoday.com"}]
    [:link {:rel "icon" :href "favicon.ico" :type "image/x-icon"}]
    [:meta {:property "og:image" :content "flattering-comparison.jpg"}]]
   [:body
    [:div {:style "max-width: 900px; margin: 40px auto;"}
    (-> data :entry :content)]]))
